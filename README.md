# Aadhaar Authentication Client #

Aadhaar Authentication Client is a nodejs based tool to test the RD Service and the authentication api.

### Prerequisites ###

  - node(6.2.2) 
  - npm(4.4.1)

### Installing Authentication Client ###
 
  - npm install 
  - node app.js

### Accessing client ui ###

  - In your web browser open http://localhost:8091


### Adhoc Test ###

![Scheme](img/adhoctest.png)

### Batch Test ###

![Scheme](img/batchtest.png)

### Windows ###

### Installing Authetication Client ###

  - Clone the repo
  - Open windows command prompt as administrator(Right click on command prompt -> Run as administrator)
  - Run npm install --add-python-to-path --global --production windows-build-tools
  - npm set config msvs_version=2015 --global
  - From inside the repo run npm install
  - node app.js