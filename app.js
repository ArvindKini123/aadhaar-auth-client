const express = require('express');
const multer = require('multer')();
const app = express();
const http = require('http');
const bodyParser = require("body-parser");
const config = require("./config");
const port = process.env.PORT || config.port;
const env = process.env.NODE_ENV || "development";
const morgan = require("morgan");
const router = express.Router();
const tests = require("./main/tests");
const helper = require("./utils/helper");
const path = require("path");


/*if ("development" === env) {
  app.use(morgan("dev"));
}*/
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json({
  limit: "5mb"
}));
app.use(express.static(path.join(__dirname, 'www')));

app.use(function(req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS,NOTIFY");
  res.setHeader("Access-Control-Allow-Headers", "accept, Content-Type, X-CAuth-Token, authorization, X-CAccess-Id, X-Signature, X-Payload, X-Login-Token");
  if ('OPTIONS' == req.method) {
    return res.send(204);
  }
  next();
});

app.put("/testFile", multer.any(), (req, res) => {
  if (!req.files || req.files.length == 0) {
    return res.status(500).send({ error: true, message: "File missing" });
  } else {
    let file = req.files[0];
    file = file.buffer.toString("utf8");
    if (!file) {
      return res.status(500).send({ error: true, message: "Empty file" });
    }
    try {
      file = JSON.parse(file);
      return tests.runTest(file)
        .then(testResult => {
          res.status(200).send(testResult);
        })
        .catch(err => {
          res.status(500).send({ error: true, message: "Error during running tests", err: { message: err.message, stack: err.stack } });
        })
    } catch (err) {
      return res.status(500).send({ error: true, message: "Corrupt json", err: { message: err.message, stack: err.stack } });
    }
  }
});

app.post("/tests", (req, res) => {
  if (!req.body) {
    return res.status(500).send({ error: true, message: "Empty data" });
  } else {
    return tests.runTest({tests:[req.body]})
      .then(testResult => {
        if(testResult.length>0){
          return res.status(200).send(testResult[0]);
        }
        res.status(500).send({ error: true, message: "Error during running tests" });  
      })
      .catch(err => {
        console.log("Err is ", err);
        res.status(500).send({ error: true, message: "Error during running tests", err: { message: err.message, stack: err.stack } });
      })
  }
});

app.post("/rdInfo", (req, res) => {
  helper.getRDInfoXML({
    rdHost:req.body.rdHost,
    rdPort:req.body.rdPort,
    rdVerb:req.body.rdVerb
  }).then(result=>{
    res.status(200).send(result);
  }).catch(err=>{
    res.status(500).send({status:"FAILED",err:{message:err.message,stack: err.stack}});
  })
});

app.post("/deviceInfo", (req, res) => {
  helper.getDeviceInfoXML({
    rdHost:req.body.rdHost,
    rdPort:req.body.rdPort,
    rdVerb:req.body.rdVerb
  }).then(result=>{
    res.status(200).send(result);
  }).catch(err=>{
    res.status(500).send({status:"FAILED",err:{message:err.message,stack: err.stack}});
  })
});

app.post("/capture", (req, res) => {
  helper.getCaptureResult({
    rdHost:req.body.rdHost,
    rdPort:req.body.rdPort,
    rdVerb:req.body.rdVerb,
    rdParams:req.body.rdParams
  }).then(result=>{
    res.status(200).send(result);
  }).catch(err=>{
    res.status(500).send({status:"FAILED",err:{message:err.message,stack: err.stack}});
  })
});

app.get("/",(req,res)=>{
  res.sendFile(path.join(__dirname + '/reactApp/src/www/index.html'));
});

app.listen(port, function() {
  console.log("RD Test server listening on port " + port + " in " + env + " mode");
});