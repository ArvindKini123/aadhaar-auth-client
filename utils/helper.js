const constants = require("../constants");
const axios = require("axios");
const async = require("asyncawait/async");
const await = require("asyncawait/await");
const R = require("ramda");
const x2js = require("xml2json");
const data = require("../data/data.js");
const fs = require("fs");
var readlineSync = require('readline-sync');
const path = require("path");
const signedXml = require('xml-crypto').SignedXml;
const xmlBuilder = require("xmlbuilder");
const signingKey = fs.readFileSync(path.join(__dirname, '../keys') + "/privateKey.pem");
const subjectName = "CN=Public AUA for Staging Services,OU=Staging Services,O=Public AUA,L=Bangalore,ST=KA,C=IN";
require('colors');
const xmlParser = x2js;
const config = require("../config");

const captureJson = {
  "PidOptions": {
    "ver": config.pidOptionsVersion,
    "Opts": {

    },
    "Demo": {

    },
    "CustOpts": {

    },
    "Bios": {

    }
  }
};

const pidData = {
  "Pid": {
    "ts": "",
    "ver": "",
    "wadh": "",
    "Demo": {}
  }
}

const getRDHost = (test) => {
  return test.rdHost || constants.RDHOST;
};

const getRDPort = (test) => {
  return test.rdPort || constants.RDPORT;
};

const getRDVerb = (test) => {
  return test.rdVerb || constants.RDVERB;
};

const getDeviceInfoVerb = (test) => {
  return test.deviceInfoVerb || constants.DEVICEINFOVERB;
};

const getCaptureVerb = (test) => {
  return test.captureVerb || constants.CAPTUREVERB;
};

const getRDInfo = (test) => {
  console.log("Begin: GetRdInfo".blue)
  let timeout = R.path(["options", "timeout"], test);
  return axios.request({
    url: "http://" + getRDHost(test) + ":" + getRDPort(test),
    method: getRDVerb(test),
    headers: {
      "Content-Type": "text/xml",
      "Accept": "text/xml"
    },
    timeout: timeout,
  });
};

const updateDB = (test) => {
  let timeout = R.path(["options", "timeout"], test);
  let type = R.path(["entity"], test) || "CACertificate";
  let id = R.path(["status"], test) || "ACTIVE";
  let host = config.dbHost + type + "/" + id;
  return axios.request({
    url: host,
    method: "POST",
    timeout: timeout,
    data: JSON.stringify(test.body)
  });
}

const getAuthUrl = (test) => {
  let uid = R.path(["authParams", "uid"], test) || "";
  let host = R.path(["options", "authHost"], test) || config.authHost;
  let asalk = R.path(["options", "asalk"], test) || config.asaLicenseKey;
  let url = host + "/public/" + uid[0] + "/" + uid[1] + "/" + asalk;
  return url;
}

const getAuthResult = (authXML, test) => {
  console.log("Begin: Authentication".blue);
  let timeout = R.path(["options", "timeout"], test);
  return axios.request({
    url: getAuthUrl(test),
    method: "POST",
    headers: {
      "Content-Type": "text/xml",
      "Accept": "text/xml"
    },
    data: authXML,
    timeout: timeout,
  });
};

const xmlify = obj => {
  let keys = Object.keys(obj);
  for (var i = 0; i < keys.length; i++) {
    let key = keys[i];
    obj["_" + key] = obj[key];
    delete obj[key];
  }
}

const getDeviceInfo = (test, infoPath) => {
  console.log("Begin: Get_DeviceInfo".blue);
  let timeout = R.path(["options", "timeout"], test);
  return axios.request({
    url: "http://" + getRDHost(test) + ":" + getRDPort(test) + infoPath,
    method: getDeviceInfoVerb(test),
    headers: {
      "Content-Type": "text/xml",
      "Accept": "text/xml"
    },
    timeout: timeout
  });
};

const capture = (test, capturePath, captureXML) => {
  console.log("Begin: Capture".blue);
  let timeout = R.path(["options", "timeout"], test);
  return axios.request({
    url: "http://" + getRDHost(test) + ":" + getRDPort(test) + capturePath,
    method: getCaptureVerb(test),
    headers: {
      "Content-Type": "text/xml",
      "Accept": "text/xml"
    },
    data: captureXML,
    timeout: timeout,
  });
};

const getInfoPath = rdInfo => {
  for (var i = 0; i < rdInfo.RDService.Interface.length; i++) {
    if (rdInfo.RDService.Interface[i].id == "DEVICEINFO") {
      return rdInfo.RDService.Interface[i].path;
    }
  }
  return null;
}

const getCapturePath = rdInfo => {
  for (var i = 0; i < rdInfo.RDService.Interface.length; i++) {
    if (rdInfo.RDService.Interface[i].id == "CAPTURE") {
      return rdInfo.RDService.Interface[i].path;
    }
  }
  return null;
}

const getDeviceInfoXML = async((test) => {
  try {
    let rdInfo = await (getRDInfo(test));
    if (!rdInfo) {
      return { status: constants.FAILED, reason: constants.reasons.EmptyRDInfo };
    }
    rdInfoXML = rdInfo.data;
    rdInfo = JSON.parse(x2js.toJson(rdInfo.data));
    let infoPath = getInfoPath(rdInfo);
    if (!infoPath) {
      return { status: constants.FAILED, reason: constants.reasons.MissingRDInfoPath, rdInfoXML: rdInfoXML };
    }
    let deviceInfo = await (getDeviceInfo(test, infoPath));
    if (!deviceInfo || !deviceInfo.data) {
      return { status: constants.FAILED, reason: constants.reasons.EmptyDeviceInfo };
    }
    return { status: constants.SUCCESS, deviceInfoXML: deviceInfo.data, rdInfoXML: rdInfoXML }
  } catch (err) {
    return { status: constants.FAILED, reason: constants.reasons.GenericDeviceInfoFailed, err: { message: err.message, stack: err.stack } };
  }
});

const getRDInfoXML = async(test => {
  try {
    let rdInfo = await (getRDInfo(test));
    if (!rdInfo || !rdInfo.data) {
      return { status: constants.FAILED, reason: constants.reasons.EmptyRDInfo };
    }
    let rdJson  = xmlParser.toJson(rdInfo.data);
    return { status: R.path(["RDService", "status"], JSON.parse(rdJson)) || constants.SUCCESS,
      rdInfoXML: rdInfo.data }
  } catch (err) {
    return { status: constants.FAILED, reason: constants.reasons.GenericRDInfoFailed, err: { message: err.message, stack: err.stack } };
  }
});

const getCaptureXML = (test) => {
  let payload = R.clone(captureJson);
  payload["PidOptions"]["ver"] = R.path(["rdParams", "ver"], test) || payload["PidOptions"]["ver"];
  payload["PidOptions"]["Opts"] = R.merge({"env":"S"},R.path(["rdParams", "Opts"], test) || {});
  payload["PidOptions"]["Demo"] = R.path(["rdParams", "Demo"], test) || {};
  payload["PidOptions"]["Bios"] = R.path(["rdParams", "Bios"], test) || {};
  payload["PidOptions"]["CustOpts"] = R.path(["rdParams", "CustOpts"], test) || {};
  return x2js.toXml(payload);
};

const getCaptureResult = async((test, pause) => {
  try {
    let rdInfo = await (getRDInfo(test));
    if (!rdInfo) {
      return { status: constants.FAILED, reason: constants.reasons.EmptyRDInfo };
    }
    rdInfoXML = rdInfo.data;
    rdInfo = JSON.parse(x2js.toJson(rdInfo.data));
    let capturePath = getCapturePath(rdInfo);
    if (!capturePath) {
      return { status: constants.FAILED, reason: constants.reasons.MissingRDCapturePath, rdInfoXML: rdInfoXML };
    }
    let captureXML = test.captureXML || getCaptureXML(test);
    if (pause) {
      readlineSync.question("Please make the backend changes and press any key to continue".green)
    }
    let captureResult = await (capture(test, capturePath, captureXML));
    if (!captureResult || !captureResult.data) {
      return {
        status: constants.FAILED,
        reason: constants.reasons.EmptyCaptureResult,
        captureRequestXML: captureXML
      };
    }
    return { status: constants.SUCCESS, captureResultXML: captureResult.data, captureRequestXML: captureXML }
  } catch (err) {
    return { status: constants.FAILED, reason: constants.reasons.GenericCaptureFailed, err: { message: err.message, stack: err.stack } };
  }
});

const getPidData = async(test => {
  let pidOptionsXml = getCaptureXML(test);
  let pidDataXml = "";
  let captureResult = await (getCaptureResult(test));
  if (captureResult.status == constants.SUCCESS) {
    pidDataXml = captureResult.captureResultXML;
  } else {
    throw new Error(captureResult);
  }
  let pidDataJson = JSON.parse(xmlParser.toJson(pidDataXml));
  let data = {};
  data.Hmac = {
    "$t":R.path(["PidData", "Hmac"], pidDataJson)
  }
  data.Skey = R.path(["PidData", "Skey"], pidDataJson);
  data.Data = R.path(["PidData", "Data"], pidDataJson);
  data.Meta = R.path(["PidData", "DeviceInfo"], pidDataJson) || {};
  return {pidOptionsXml:pidOptionsXml,pidDataXml:pidDataXml,data:data};
});

const hasBio = test => {
  return getBt(test).length > 0;
}

const getTid = test => {
  return hasBio(test) ? "registered" : "registered"
}

const getBt = test => {
  const ftypes = ["FMR", "FIR"];
  const iTypes = ["IIR"];
  let bt = "";
  if (R.path(["rdParams", "Opts", "fType"], test) != null) {
    bt = bt + ftypes[parseInt(R.path(["rdParams", "Opts", "fType"], test))]
  }
  if (R.path(["rdParams", "Opts", "iType"], test) != null) {
    bt = bt + iTypes[parseInt(R.path(["rdParams", "Opts", "iType"], test))]
  }
  if (R.path(["rdParams", "Opts", "pType"], test) != null) {
    bt = bt + R.path(["rdParams", "Opts", "pType"], test)
  }
  return bt;
}

const getUses = test => {
  let uses = R.clone(data.uses);
  uses.pa = R.path(["authParams", "Uses","pa"], test) || (R.path(["rdParams", "Demo", "Pa"], test) ? "y" : "n");
  uses.pfa = R.path(["authParams", "Uses","pfa"], test) || (R.path(["rdParams", "Demo", "Pfa"], test) ? "y" : "n");
  uses.pi = R.path(["authParams", "Uses","pi"], test) || (R.path(["rdParams", "Demo", "Pi"], test) ? "y" : "n");
  uses.bt = R.path(["authParams", "Uses","bt"], test) || getBt(test);
  uses.bio = R.path(["authParams", "Uses","bio"], test) || (uses.bt.length > 0 ? "y" : "n");
  uses.pin = R.path(["authParams", "Uses","pin"], test) || (R.path(["rdParams", "Demo", "pin"], test) ? "y" : "n");
  uses.otp = R.path(["authParams", "Uses","otp"], test) || (R.path(["rdParams", "Opts", "otp"], test) ? "y" : "n");
  return uses;
}

const signXml = (authXml, test) => {
  console.log("Begin: SignXML".blue)
  var sig = new signedXml();
  sig.addReference("//*[local-name(.)='Auth']", ['http://www.w3.org/2000/09/xmldsig#enveloped-signature', 'http://www.w3.org/2001/10/xml-exc-c14n#'], 'http://www.w3.org/2001/04/xmlenc#sha256',null, null, null, true);
  let keyPath = R.path(["options", "privateKeyPath"], test) || path.join(__dirname, '../keys') + "/privateKey.pem";
  if(!fs.existsSync(keyPath)){
    throw Error("Invalid Private key Path");
  }
  let signingKey = fs.readFileSync(keyPath).toString();
  sig.signingKey = signingKey;
  sig.canonicalizationAlgorithm = 'http://www.w3.org/2001/10/xml-exc-c14n#';
  sig.signatureAlgorithm = "http://www.w3.org/2000/09/xmldsig#rsa-sha1";
  sig.keyInfoProvider = new KeyInfo(test);
  sig.computeSignature(authXml);
  return sig.getSignedXml();
}

function KeyInfo(test) {
  let certPath = R.path(["options", "certPath"], test) || path.join(__dirname, '../keys') + "/cert";
  if(!fs.existsSync(certPath)){
    throw Error("Invalid cert Path");
  }
  let cert = fs.readFileSync(certPath).toString();
  this.getKeyInfo = () => {
    return xmlBuilder.create('X509Data', { version: '1.0', encoding: 'UTF-8', standalone: true }, { pubID: null, sysID: null }, {
        allowSurrogateChars: false,
        skipNullAttributes: false,
        headless: true,
        ignoreDecorators: false,
        separateArrayItems: false,
        noDoubleEncoding: false,
        stringify: {}
      })
      .ele('X509SubjectName', null, subjectName)
      .up()
      .ele('X509Certificate', null, cert)
      .end();
  }
  this.getKey = (keyInfo) => {}
}

const setAuthProperties = (test,authData) => {
  authData.Auth.uid = R.path(["authParams", "uid"], test) || "";
  authData.Auth.rc = R.path(["authParams", "rc"], test) || config.rc;
  authData.Auth.txn = R.path(["authParams", "txn"], test) || new Date().getTime();
  authData.Auth.tid = R.path(["authParams", "tid"], test) || getTid(test);
  authData.Auth.ac = R.path(["authParams", "ac"], test) || authData.Auth.ac;
  authData.Auth.sa = R.path(["authParams", "sa"], test) || authData.Auth.sa;
  authData.Auth.ver = R.path(["authParams", "ver"], test) || authData.Auth.ver;
  authData.Auth.lk = R.path(["authParams", "lk"], test) || authData.Auth.lk;
  return authData;
}

const getBioAuthResult = async(test => {
  try {
    let pidDataResult = await (getPidData(test));
    let pidData = pidDataResult.data;
    let authData = R.clone(data.authJson);
    authData = setAuthProperties(test,authData);
    pidData.Meta.udc = R.path(["authParams", "Meta","udc"], test) || authData.Auth.Meta.udc;
    authData.Auth = R.merge(authData.Auth,pidData);
    authData.Auth.Uses = getUses(test, pidData);
    let authXml = xmlParser.toXml(authData);
    authXml = '<?xml version="1.0" encoding="UTF-8"?>'+authXml;
    authXml = signXml(authXml, test);
    let authResult = await (getAuthResult(authXml, test));
    if (!authResult || !authResult.data) {
      return { status: constants.FAILED, reason: constants.reasons.EmptyAuthResult,
        pidOptionsXml:pidDataResult.pidOptionsXml,pidDataXml:pidDataResult.pidDataXml};
    }
    return { status: constants.SUCCESS, authResponseXML: authResult.data,authRequestXML: authXml,
      pidOptionsXml:pidDataResult.pidOptionsXml,pidDataXml:pidDataResult.pidDataXml}
  } catch (err) {
    return { status: constants.FAILED, reason: constants.reasons.GenericAuthFailed, err: { message: err.message, stack: err.stack } };
  }
});


exports.signXml = signXml;
exports.getCaptureResult = getCaptureResult;
exports.getDeviceInfoXML = getDeviceInfoXML;
exports.getRDInfoXML = getRDInfoXML;
exports.getAuthResult = getAuthResult;
exports.getBioAuthResult = getBioAuthResult;
exports.updateDB = updateDB;