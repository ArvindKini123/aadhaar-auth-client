const axios = require("axios");
const async = require("asyncawait/async");
const await = require("asyncawait/await");
const constants = require("../constants");
const helper = require("../utils/helper");

const deviceInfo = async(test => {
	return await(helper.getDeviceInfoXML(test));
})

const captureBiometric = async(test => {
  return await(helper.getCaptureResult(test));
});

const captureBiometricWait = async(test => {
  return await(helper.getCaptureResult(test, true));
});

const getRDInfoXML = async(test => {
	return await(helper.getRDInfoXML(test));
});

const verifyDemo = async(test => {
	return await(helper.getBioAuthResult(test));
});

const verifyBio = async(test => {
	return await(helper.getBioAuthResult(test));
});

const updateDB = async(test => {
  return await(helper.updateDB(test));
});

exports.deviceInfo = deviceInfo;
exports.captureBiometric = captureBiometric;
exports.captureBiometricWait = captureBiometricWait;
exports.getRDInfoXML = getRDInfoXML;
exports.verifyDemo = verifyDemo;
exports.verifyBio = verifyBio;
exports.updateDB = updateDB;