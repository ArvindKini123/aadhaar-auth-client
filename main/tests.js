const processors = require("./processors");
const assertions = require("./assertions");
const async = require("asyncawait/async");
const await = require("asyncawait/await");
const constants = require("../constants");
const R = require("ramda");
const xmlParser = require("xml2json");

const tests = [{
  type: "DEVICE_INFO",
  processor: processors.deviceInfo,
  assert: assertions.deviceInfoSuccess
}, {
  type: "DEVICE_INFO_NEGATIVE",
  processor: processors.deviceInfo,
  assert: assertions.deviceInfoNegative
}, {
  type: "RD_CAPTURE",
  processor: processors.captureBiometric,
  assert: assertions.captureBiometricSuccess
}, {
  type: "RD_CAPTURE_WAIT",
  processor: processors.captureBiometricWait,
  assert: assertions.captureBiometricSuccess
}, {
  type: "RD_CAPTURE_NEGATIVE",
  processor: processors.captureBiometric,
  assert: assertions.captureBiometricNegative
}, {
  type: "RD_INFO",
  processor: processors.getRDInfoXML,
  assert: assertions.rdInfoSuccess
}, {
  type: "RD_INFO_NEGATIVE",
  processor: processors.getRDInfoXML,
  assert: assertions.rdInfoNegative
}, {
  type: "AUTH",
  processor: processors.verifyBio,
  assert: assertions.verifyAuth
}, {
  type: "UPDATE_DB",
  processor: processors.updateDB,
  assert: assertions.updateDB
}];

const customAssert = (testcase, result) => {
  try{
    let resp;
    if(Object.keys(result).length == 1){
      resp  = result[Object.keys(result)[0]];
    }else{
      resp = result[Object.keys(result)[1]];
    }
    let assert = testcase.assertCondition;
    let response = JSON.parse(xmlParser.toJson(resp));
    let comparator = (a,b)=>{
      let akeys = Object.keys(a);
      let bkeys = Object.keys(b);
      for(let i in bkeys){
        let key = bkeys[i];
        if(typeof b[key] == typeof a[key] && typeof b[key] == "object"){
          if(!comparator(a[key], b[key]))
            return false;
        }else{
          if(!(a[key]===b[key]))
            return false;
        }
      }
      return true;
    };
    if(comparator(response, assert))
      return { status: constants.SUCCESS };
    else
      return { status: constants.FAILED, reason: "Custom assertion failed" };
  }catch(e){
     return {
      status: constants.FAILED,
      reason: "Custom assertion failed",
      err: { message: e.message, stack: e.stack }
    };
  }
};

const testExecutor = async(testCase => {
  let test = R.filter(x => x.type == testCase.type, tests);
  if (!test || test.length == 0) {
    return { status: constants.FAILED, reason: constants.reasons.InvalidTType };
  }
  let meta = "TestCase:" + testCase.name + " Type:" + testCase.type;
  console.log("-------------------\n" + meta.green);
  test = test[0];
  let processorResult = await (test.processor(testCase));
  if (!processorResult) {
    console.log("-------------------\n" + "TestCase Completed Status=Failed".red);
    return { status: constants.FAILED, reason: constants.reasons.EmptyPResponse };
  }
  let assert = null;
  if (testCase.assertCondition) {
    assert = customAssert(testCase, processorResult);
  } else {
    assert = test.assert(testCase, processorResult);
  }
  if (!assert) {
    console.log("-------------------\n"+"TestCase Completed Status=Failed".red);
    return { status: constants.FAILED, reason: constants.reasons.EmptyAssertResponse };
  }
  console.log("TestCase Completed".green +  "\n-------------------");
  return R.merge(testCase, { assertResponse: assert, executorResponse: processorResult });
});

const runTest = async(testInfo => {
  let testCases = testInfo.tests;
  testCases = R.map(testCase => {
    return R.merge(testInfo.globals, testCase);
  }, testCases);
  let results = [];
  let i = 0;

  for (i = 0; i < testCases.length; i++) {
    try {
      results.push(await(testExecutor(testCases[i])))
    } catch (err) {
      results.push(R.merge({
        status: constants.FAILED,
        reason: constants.reasons.GenericTestFailure,
        err:{message:err.message,stack:err.stack}
      }, testCases[i]));
    }
  }
  return results;
})

exports.runTest = runTest;
exports.testExecutor = testExecutor;