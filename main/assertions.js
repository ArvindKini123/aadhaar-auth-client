const constants = require("../constants");
const R = require("ramda");
const xmlParser = require("xml2json");
const data = require("../data/data");
const config = require("../config.js");

const deviceInfoSuccess = (test, processorResult) => {
  if (!processorResult) {
    return { status: constants.FAILED, reason: constants.reasons.EmptyDeviceInfo }
  }
  if (processorResult.deviceInfoXML) {
    try {
      let deviceInfoJson = JSON.parse(xmlParser.toJson(processorResult.deviceInfoXML));
      return assertDeviceInfo(test, deviceInfoJson.DeviceInfo);
    } catch (err) {
      return {
        status: constants.FAILED,
        reason: constants.reasons.GenericAuthFailed,
        err: { message: err.message, stack: err.stack }
      };
    }
  } else {
    return {
      status: constants.FAILED,
      reason: processorResult.reason ||
        constants.reasons.EmptyDeviceInfo
    };
  }
}

const assertDeviceInfo = (test, deviceInfoJson) => {
  for (let deviceInfoKey of data.deviceInfoAssertKeys) {
    let key = deviceInfoKey.key;
    if (!matchProperty(deviceInfoJson[key],
        R.path(["assertCondition", "deviceInfo", key], test), deviceInfoKey.defaultValue)) {
      return {
        status: constants.FAILED,
        reason: "Error matching property " + key
      };
    }
  }
  return { status: constants.SUCCESS }
}

const matchProperty = (value, expected, defaultValue) => {
  if (expected != null) {
    return (new RegExp(expected)).test(value);
  }
  return new RegExp(defaultValue).test(value);
}

const rdInfoSuccess = (test, processorResult) => {
  if (!processorResult) {
    return { status: constants.FAILED, reason: constants.reasons.EmptyRDInfo }
  }
  if (processorResult.rdInfoXML) {
    return { status: constants.SUCCESS };
  } else {
    return {
      status: constants.FAILED,
      reason: processorResult.reason ||
        constants.reasons.EmptyRDInfo
    };
  }
}

const updateDB = (test, processorResult) => {
  if (!processorResult) {
    return { status: constants.FAILED, reason: constants.reasons.EmptyRDInfo }
  }
  else {
    return { status: constants.SUCCESS };
  }
}

const rdInfoNegative = (test, processorResult) => {
  if (!processorResult) {
    return { status: constants.FAILED, reason: constants.reasons.EmptyRDInfo }
  }
  if (!processorResult.rdInfoXML) {
    return { status: constants.SUCCESS, reason: "" };
  } else {
    return { status: constants.FAILED, reason: processorResult.reason };
  }
}

const captureBiometricSuccess = (test, processorResult) => {
  if (!processorResult) {
    return { status: constants.FAILED, reason: constants.reasons.EmptyCaptureResult }
  }
  if (processorResult.captureResultXML) {
    try {
      let captureJson = JSON.parse(xmlParser.toJson(processorResult.captureResultXML));
      return assertCaptureDetails(test, captureJson.PidData);
    } catch (err) {
      return {
        status: constants.FAILED,
        reason: constants.reasons.GenericCaptureFailed,
        err: { message: err.message, stack: err.stack }
      };
    }
  } else {
    return {
      status: constants.FAILED,
      reason: processorResult.reason ||
        constants.reasons.EmptyCaptureResult
    };
  }
}

const assertCaptureDetails = (test, captureJson) => {
  if (R.path(["assertCondition", "captureResult", "Resp", "errCode"], test)) {
    if (R.path(["assertCondition", "captureResult", "Resp", "errCode"], test) == R.path(["Resp", "errCode"], captureJson)) {
      return { status: constants.SUCCESS };
    } else {
      return { status: constants.FAILED, reason: constants.reasons.ErrorCodeMismatch };
    }
  } else {
    if (R.path(["Resp", "errCode"], captureJson) == config.captureRespErrorCode) {
      return { status: constants.SUCCESS };
    } else {
      return { status: constants.FAILED, reason: constants.reasons.ErrorCodeMismatch };
    }
  }
}

const deviceInfoNegative = (test, processorResult) => {
  if (!processorResult) {
    return { status: constants.FAILED, reason: constants.reasons.EmptyDeviceInfo }
  }
  if (!processorResult.deviceInfoXML) {
    return { status: constants.SUCCESS, reason: "" };
  } else {
    return { status: constants.FAILED, reason: processorResult.reason };
  }
}

const captureBiometricNegative = (test, processorResult) => {
  if (!processorResult) {
    return { status: constants.FAILED, reason: constants.reasons.EmptyCaptureResult }
  }
  let data = JSON.parse(xmlParser.toJson(processorResult.captureResultXML));
  if (R.path(["PidData", "Resp", "errCode"], data)) {
    return { status: constants.SUCCESS, reason: "" };
  } else {
    return { status: constants.FAILED, reason: processorResult.reason };
  }
}

const authErr = (test) => {
  return R.path(["assertCondition", "auth", "err"], test);
}

const authRet = (test, authResultJson) => {
  return R.path(["assertCondition", "auth", "ret"], test) || config.ret;
}

const verifyAuth = (test, processorResult) => {
  if (!processorResult) {
    return { status: constants.FAILED, reason: constants.reasons.EmptyAuthResult }
  }
  if (processorResult.authResponseXML) {
    try {
      let authResultJson = JSON.parse(xmlParser.toJson(processorResult.authResponseXML));
      if (R.path(["AuthRes", "err"], authResultJson) == authErr(test) &&
        R.path(["AuthRes", "ret"], authResultJson) == authRet(test)) {
        return { status: constants.SUCCESS }
      } else {
        return {
          status: constants.FAILED,
          reason: R.path(["AuthRes", "err"], authResultJson) + "_" + R.path(["AuthRes", "ret"], authResultJson)
        };
      }
    } catch (err) {
      return {
        status: constants.FAILED,
        reason: constants.reasons.GenericAuthFailed,
        err: { message: err.message, stack: err.stack }
      };
    }
  } else {
    return {
      status: constants.FAILED,
      reason: processorResult.reason ||
        constants.reasons.EmptyAuthResult
    };
  }
}

exports.deviceInfoSuccess = deviceInfoSuccess;
exports.deviceInfoNegative = deviceInfoNegative;
exports.captureBiometricSuccess = captureBiometricSuccess;
exports.captureBiometricNegative = captureBiometricNegative;
exports.rdInfoSuccess = rdInfoSuccess;
exports.rdInfoNegative = rdInfoNegative;
exports.verifyAuth = verifyAuth;
exports.updateDB = updateDB;
