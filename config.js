module.exports = {
  "pidOptionsVersion": "1.0",
  "deviceInfo": {
    "dpId": "qa_manufacturer",
    "rdsId": "RDSID",
    "rdsVer": "2.0_QA",
    "dc": "DFA48664F9F346149FB447AC64F4D23F",
    "mi": "POCMI",
    "mc": "MIIDojCCAoqgAwIBAgIGAVsjIgPZMA0GCSqGSIb3DQEBBQUAMHwxGDAWBgNVBAMMD3FhX21hbnVmYWN0dXJlcjEYMBYGA1UECwwPcWFfbWFudWZhY3R1cmVyMRgwFgYDVQQKDA9xYV9tYW51ZmFjdHVyZXIxEjAQBgNVBAcTCUJhbmdhbG9yZTELMAkGA1UECBMCS0ExCzAJBgNVBAYTAklOMB4XDTE3MDMzMTA2NTEyM1oXDTE4MDMzMTA2NTEyM1owajESMBAGA1UEAwwJcWFfZGV2aWNlMRIwEAYDVQQLDAlxYV9kZXZpY2UxEjAQBgNVBAoMCXFhX2RldmljZTESMBAGA1UEBxMJQmFuZ2Fsb3JlMQswCQYDVQQIEwJLQTELMAkGA1UEBhMCSU4wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDho3HKvn1yUj9hIEXtkkUeomaFgs4qbyuXpiR5gGnZ4O8JAi5fu3vi6+I8LPb4pFNnPvfdcQ1bTALJV0Rm2BjE4vDQ/d9nic8Q+ADwrDR/CKyGQuaXg5i9s/+pB9o3IF6CF1f6ttnR0U1TnAOxf6vw1igW1ISA+npEX7wukKM12HBHPvFKTU1AeRofJVzyuFq0dmhBLa/Xf6TW5ura+YZXsO4FtgRAH8Ct+imUxLmiiGyyhj3F1OJrTuWJ2M7HWWSntFu/ZCGm5Bw6Fp60hKZfEe2rMnevr2QwDFatu8RxjcMsvQL936oaUVlERRVwfn8hMVb8gIkxwrzR5xbrMKTNAgMBAAGjPDA6MAwGA1UdEwQFMAMBAf8wCwYDVR0PBAQDAgGGMB0GA1UdDgQWBBST5/B8saFsm91i+0bE/a6VIUkKeDANBgkqhkiG9w0BAQUFAAOCAQEAblitlME4CD1mbGtdD7AjyXRBFdPxUPvjmWnJzYXbg3XP8jmWQGvejCHa2lICi0ubHMkLu6Mghd8jx81B4uQNS7VsKH5yRIZadLmB5lShtoH3aaChYat7P/n7oShKBQ7yRiBro+4SWw2o3XThGcnhKL4zZhb/ot+5KwyL2AiGSwz696g/NA0l7kJzZvKDdC12ufrgIKf6Z9SUmKaltQDauaRQ4uxZU8heIa9RI3SZsv0TCEfzHns/M+P3sL690vJbLWx+uI7p6BdTKZh0QdM6J1ZF5UmKObUrZaANNsbXcwkEldjBlglzYr+qQldusanfaMLCRFBrpcDU5xRfL9XMnA=="
  },
  "udc": "200",
  "licenseKey": "MBFWjkJHNF-fLidl8oOHtUwgL5p1ZjDbWrqsMEVEJLVEDpnlNj_CZTg",
  "asaLicenseKey": "MH4hSkrev2h_Feu0lBRC8NI-iqzT299_qPSSstOFbNFTwWrie29ThDo",
  "authVersion": "2.0",
  "subAuaCode": "public",
  "auaCode": "public",
  "authHost": "http://developer.uidai.gov.in/auth",
  "dbHost": "http://developer.uidai.gov.in/amcs/objects/",
  "ret":"y",
  "captureRespErrorCode":"0",
  "rc":"Y",
  "port":"8091",
  "rdHost":"127.0.0.1",
  "rdPort":"11110",
  "rdVerb":"RDSERVICE",
  "deviceInfoVerb":"DEVICEINFO",
  "captureVerb":"CAPTURE",
  "pidVersion":"2.0"
}
